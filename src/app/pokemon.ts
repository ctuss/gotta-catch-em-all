export interface Pokemon {
    name: string
    id: number
    sprites: Sprites
    height: number
    weight: number
    base_experience: number
    abilities: Abilities[]
    types: Type[]
    moves: Move[]
    stats: Basestats[]
}

interface Sprites {
    back_default: any
    front_default: any
    back_shiny: any
    front_shiny: any
}

interface Abilities {
    slot: string
    ability: [{
        name: string
    }]
}

interface Type {
    type: {
        name: string
    }
}

interface Basestats {
    base_stat: number
    stat: {
        name: string
    }
}

interface Move {
    move: {
        name: string
    }
}
