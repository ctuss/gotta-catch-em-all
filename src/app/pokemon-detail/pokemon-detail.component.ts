import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {
  pokemon: Pokemon;

  constructor(
    private pokemonService: PokemonService,
    private route: ActivatedRoute,
    private location: Location) { }

  /**
   * When component is initialized we make a pokemon object based on name
   */
  ngOnInit() {
    this.getPokemon();
  }

  /**
   * Get pokemon from router based on name
   */
  getPokemon(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.pokemonService.getPokemon(name).subscribe(pokemon => this.pokemon = pokemon);
  }

  /**
   * Routes one page back
   */
  goBack() {
    this.location.back();
  }

}
