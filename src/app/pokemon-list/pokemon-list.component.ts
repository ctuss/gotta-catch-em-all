import { Component, OnInit, resolveForwardRef } from '@angular/core';


import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';
import { Apiresponse } from '../apiresponse';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  pokemons: Pokemon[] = [];
  pokemon: Pokemon;
  searchString: string = '';
  apiresponse: Apiresponse;
  searchResult: Pokemon[]

  //constructor
  constructor(
    private pokemonService: PokemonService) { }

  /**
   * When component initialized, checks if an array has been stored in pokemon.service,
   * if not will load a new array with the first apiresponse.
   * If an array exists it copies the stored array and the last apiresponse.
   */
  ngOnInit() {
    if (this.pokemonService.getPokemonArray().length < 1) {
      this.getApiresponse();
    }
    else {
      this.pokemonService.getPokemonArray().forEach(pokemon => {
        this.pokemons.push(pokemon)
      });
      this.apiresponse = this.pokemonService.getSavedApiresponse();
      this.pokemonService.resetPokemonArray();
      this.searchForPokemons();
    }
  }

  /**
   * When component is destroyed stores last apiresponse and pokemonarray into pokemon.service
   */
  ngOnDestroy() {
    this.pokemonService.addPokemons(this.pokemons);
    this.pokemonService.addApiresponse(this.apiresponse);
  }

  /**
   * Click function for button to loadmore pokemons
   */
  onClick() {
    this.getNextApiresponse();
  }

  
  getApiresponse(): void {
    this.pokemonService.getApiresponse().subscribe(apiresponse => {
      this.apiresponse = apiresponse,
        this.getPokemons()
    });
  }

  searchForPokemons(): void {
    this.searchResult = this.pokemons.filter(poke => {
      return poke.name.includes(this.searchString.toLowerCase());
    })
    console.log(this.searchResult);
  }

  getNextApiresponse(): void {
    this.pokemonService.getNextApiresponse(this.apiresponse.next)
      .subscribe(apiresponse => {
        this.apiresponse = apiresponse,
          this.getPokemons()
      });
  }

  getPokemons(): void {
    this.apiresponse.results.forEach(poke => {
      this.pokemons.push(poke);

      this.searchForPokemons();
    });
  }
  //not in use
  fixingCIBuild() {
  }

  fixingCIBuildAgain() {
    
  }
}

