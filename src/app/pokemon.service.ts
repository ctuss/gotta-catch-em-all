import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Pokemon } from './pokemon';
import { Observable, of } from 'rxjs';
import { catchError, map, tap, delay } from 'rxjs/operators';
import { Apiresponse } from './apiresponse';
import { MessageService } from './message.service';


@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private pokemonUrl = 'https://pokeapi.co/api/v2/pokemon';
  apiresponse: Apiresponse
  pokemons: Pokemon[] = [];
  pokemon: Pokemon
  loaded: boolean = false

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  private log(message: string) {
    this.messageService.add(`Pokemonservice: ${message}`);
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  /**
   * GET initial apiresponse from API
   */
  getApiresponse(): Observable<Apiresponse> {
    return this.http.get<Apiresponse>(this.pokemonUrl)
      .pipe(
        catchError(this.handleError<Apiresponse>('getApiresponse: ')),
        tap(_ => this.log('klarte det'))
      );
  }

  /**
   * Getting the next 20 pokemons from API
   */
  getNextApiresponse(url): Observable<Apiresponse> {
    return this.http.get<Apiresponse>(url)
      .pipe(
        catchError(this.handleError<Apiresponse>('getNextApiresponse: ')),
        //tap(_ => this.log('klarte det'))
      );
  }

  getPokemon(name: string): Observable<Pokemon> {
    const url = `${this.pokemonUrl}/${name}`;
    return this.http.get<Pokemon>(url)
      .pipe(
        //tap(_ => this.log(`fetched pokemon id=${name}`)),
        catchError(this.handleError<Pokemon>(`getPokemon id=${name}`))
      );

  }
  /**
   * Stores pokemon array from pokemon-list.component
   */
  addPokemons(pokemonArray): void {
    this.loaded = true
    pokemonArray.forEach(pokemon => {
      this.pokemons.push(pokemon);
    });
  }

  /**
   * Reset pokemon array
   */
  resetPokemonArray(): void {
    this.pokemons = []
  }
  /**
   * Returns saved pokemon array
   */
  getPokemonArray(): Pokemon[] {
    return this.pokemons;
  }
  
  /**
   * Stores the last apireponse
   */
  addApiresponse(apiresponse): void {
    this.apiresponse = apiresponse;
  }

  /**
   * Returns last apiresponse
   */
  getSavedApiresponse(): Apiresponse {
    return this.apiresponse;
  }

  /**
   * Error handler
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
}