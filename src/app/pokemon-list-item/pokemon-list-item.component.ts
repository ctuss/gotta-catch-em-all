import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})

export class PokemonListItemComponent implements OnInit {
  @Input() pokemon: Pokemon

  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
    this.getPokemon(this.pokemon.name);
  }

  getPokemon(name): void {
    this.pokemonService.getPokemon(name).subscribe(pokemon => { 
      this.pokemon = pokemon,
    console.log('DATA: ', this.pokemon)});
  }

}
