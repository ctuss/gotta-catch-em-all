import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';

@Component({
  selector: 'app-pokemon-moves',
  templateUrl: './pokemon-moves.component.html',
  styleUrls: ['./pokemon-moves.component.css']
})
export class PokemonMovesComponent implements OnInit {
  @Input() pokemon: Pokemon

  constructor() { }

  ngOnInit() {
    this.pokemon.moves.forEach(move => {
      move.move.name = move.move.name.replace(/-/g, ' ');
    })
  }

}
