# MyAngularProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

# Components

## Pokemon-List
Creates a array of pokemons from API call. When you load more pokemons it pushes the new pokemons to the existing array. Passes a pokemon to pokemon-list-item.component.

### Pokemon-List-item
Creates a card for each pokemon and displays name and picture. For each card created it makes a API call
to fetch picture based on name.

## Pokemon-Detail
Creates a card based on routed name from pokemon-list.component. Creates a object of pokemon from name and displays a picture of a pokemon. Then sends object to other child components. 

### Pokemon-Profile
Displays height, weight, exp, sprites and abilities of a pokemon.

### Pokemon-Stats
Displays all stats and attributes of a pokemon.

### Pokemon-Moves
Displays all teachable moves for a pokemon.

## Pokemon-service
Core service of the application. Handles API calls and also stores the array of pokemons and latest apiresponse.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

