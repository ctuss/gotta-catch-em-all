'use strict'

/**
 * Dependencies
 * @ignore
 */
const express = require('express')
const morgan = require('morgan')
const compression = require('compression');
/**
 * App
 * @ignore
 */
const _app_folder = 'dist/pokemon-app';
const app = express()

app.use(morgan('tiny'))
app.use(compression());

app.get('*.*', express.static(_app_folder, { maxAge: '1y'}));

app.all('*', function(req, res) {
    res.status(200).sendFile(`/`, { root: _app_folder});
})

app.listen(process.env.PORT || 4200, () =>
    console.log(`Listening on port ${process.env.port || 3000}`)
);